package com.example.kevin.workmanager

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.work.Constraints
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val constraint = Constraints.Builder().setRequiresBatteryNotLow(true).build()
        val workRequest = PeriodicWorkRequest.Builder(MyWorker::class.java,20,TimeUnit.MINUTES)
            .setConstraints(constraint)
            .build()
        WorkManager.getInstance().enqueue(workRequest)
//        findViewById<Button>(R.id.btnEnqueue).setOnClickListener{
//            WorkManager.getInstance().enqueue(workRequest)
//        }
    }
}
